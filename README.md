# cloudsec-test-assignment

## INTRO

This task challenges you to create an AWS baseline security configuration withing your own account.
Our intention behind this task is to have something to talk about in an interview, so it does not matter if you get stuck and cannot complete it: just deliver whatever you got.

## Required

1. Register AWS account. You can use your own account if it exists 
2. Configure AWS account with reference to IAM Security best practices. Consider the applicable options for root account hardening on your behave. Be ready to talk about IAM and security principles there.
3. Activate AWS Security Hub, GuardDuty, Config, IAM Access Analyzer. Be ready to talk about these services philosophy. 
4. In AWS Security Hub enable baseline security checks (CIS AWS Foundations / AWS Foundational Security Best Practices). Be ready to talk about the differences and rules coverage in the standards.
5. Configure Guardduty event collection in Security Hub.
6. Run publicly available EC2 machine in AWS. Be ready to talk about firewall types and network security principles for the instances (high level).
7. Create a AWS S3 bucket. 
8. Emulate an attack by each resource type GuardDuty (1 for EC2 finding types, 1 for IAM finding types, S3 finding types). Be ready to talk about the differrences.
9. Deliver simulated alerts from Security hub to Email/Slack/etc.

***

## Optional^*^:  

+ AWS Elasticsearch. Do we need additional log storage? Where? Why?
+ Describe all the stack via terraform code.
+ Configure CI Pipeline for all infrastructure deployment

### Don't hesitate to contact and ask any question...